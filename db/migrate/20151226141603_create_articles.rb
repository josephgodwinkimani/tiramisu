class CreateArticles < ActiveRecord::Migration
	#-----------------------This is a Database migration file-----------
	# Lets Run a Migration!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	# run this command in your terminal:

    # $ bin/rails generate model Article title:string text:text
	#=======================================================================================
	# bin/rails generate model created a database migration file inside the db/migrate directory. 
	# Migrations are Ruby classes that are designed to make it simple to create and modify database tables. 
	# Rails uses rake commands to run migrations, and it's possible to undo a migration after it's been applied to your database. 
	# Migration filenames include a timestamp to ensure that they're processed in the order that they were created.
	
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text

      t.timestamps null: false
    end
  end
    #---------------------------
	# The above migration creates a method named change which will be called when you run this migration.
	# The action defined in this method is also reversible, which means Rails knows how to reverse the change made by this migration, in case you want to reverse it later. 
	# When you run this migration it will create an articles table with one string column and a text column. 
	# It also creates two timestamp fields to allow Rails to track article creation and update times.
	
	# At this point, you can use a rake command to run the migration:
	# $ bin/rake db:migrate
	
	# Because you're working in the development environment by default, this
	# command will apply to the database defined in the development section of your
	# config/database.yml file. If you would like to execute migrations in another
	# environment, for instance in production, you must explicitly pass it when
	# invoking the command: bin/rake db:migrate RAILS_ENV=production.
	
	# In development mode (which is what you're working in by default), Rails reloads your application with every browser request, 
	# so there's no need to stop and restart the web server when a change is made.
	
	
	
end
		# More Information about ACTIVE RECORD MIGRATIONS.
	 
	  # Migrations are a feature of Active Record that allows you to evolve your database schema over time.

	  # Rather than write schema modifications in pure SQL, migrations allow you to use an easy Ruby DSL to describe changes to your tables.

	  # Migrations are a convenient way to alter your database schema over time in a consistent and easy way. They use a Ruby DSL so that you don't have to write SQL by hand, allowing your schema and changes to be database independent.

		# You can think of each migration as being a new 'VERSION' of the database. 
		# A schema starts off with nothing in it, and each migration modifies it to add or remove tables, columns, or entries. 
		# Active Record knows how to update your schema along this timeline, bringing it from whatever point it is in the history to the latest version.
		# Active Record will also update your db/schema.rb file to match the up-to-date structure of your database.		