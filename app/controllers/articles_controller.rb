class ArticlesController < ApplicationController
	# A frequent practice is to place the standard CRUD actions in each CONTROLLER in the following order: 
	# index, show, new, edit, create, update and destroy. 
	
    http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
	
	# block access to the various actions if the person is not authenticated. 
	# Here we use the Rails http_basic_authenticate_with method, which allows access to the requested action if that method allows it.
	
	
	
	
	#-----------------INTRODUCTION -----------------------------------
	# A controller is simply a class that is defined to inherit from ApplicationController. 
	# It's inside this class that you'll define methods that will become the actions for this controller. 
	# These actions will perform CRUD operations on the articles within our system.
	# There are public, private and protected methods in Ruby,
    # but only public methods can be actions for controllers.
	
	#----------------------STEP 4 ------------------------------------
	# Add the corresponding index action for that route inside the ArticlesController in the app/controllers/articles_controller.rb file. 
	# When we write an index action, the usual practice is to place it as the first method in the controller. Let's do it:
	def index
    @articles = Article.all
	end
	
	#---------------------STEP 3 -------------------------------------------------------------------
	# show action is here:
		def show
			@article = Article.find(params[:id])
			
			
		end
		
	# Make sure the above keyword_end are aligned as above or you will run into a syntax error.
	


 	# --------------------STEP 1 -------------------------------------
	# There are public, private and protected methods in Ruby,
    # but only public methods can be actions for controllers so that the controller now looks like this:
	def new
		 @article = Article.new
	end
	# snippet for brevity
	
	
	#----------------------STEP 4--------------------------------------
	# Now let's focus on the "U" part, updating articles. The first step we'll take is adding an edit action to the ArticlesController, 
	# generally between the new and create actions. Subsequently, create an edit.html.erb
	
	def edit
         @article = Article.find(params[:id])
    end
	
	
	#----------------------STEP 2--------------------------------------
	# Define a create action within the ArticlesController class-
	# in app/controllers/articles_controller.rb, underneath the new action
	# What the create action should be doing is saving our new article to the database.
	# When a form is submitted, the fields of the form are sent to Rails as parameters. 
	# These parameters can then be referenced inside the controller actions, typically to perform a particular task.
	# The render method here is taking a very simple hash with a key of plain and value of params[:article].inspect. 
	# The params method is the object which represents the parameters (or fields) coming in from the form. 
	# The params method returns an ActiveSupport::HashWithIndifferentAccess object, which allows you to access the keys of the hash using either strings or symbols. 
	# In this situation, the only parameters that matter are the ones from the form.
	
	
	def create
		@article = Article.new(article_params)
 
		if @article.save
			redirect_to @article
		else
			render 'new'
		
		end
        end	
	# The new action is now creating a new instance variable called @article, and you'll see why that is in just a few moments.
	# Notice that inside the create action we use render instead of redirect_to when save returns false. 
	# The render method is used so that the @article object is passed back to the new template when it is rendered. 
	# This rendering is done within the same request as the form submission.
	
	
	
	#---------------------STEP 5 -------------------------------------------------------------------
	#
	def update
		@article = Article.find(params[:id])
 
		if @article.update(article_params)
			redirect_to @article
		else
			render 'edit'
	end
	end
	# The new method, update, is used when you want to update a record that already exists, and it accepts a hash containing the attributes 
	# that you want to update. As before, if there was an error updating the article we want to show the form back to the user.
	# You don't need to pass all attributes to update. For example, if you'd call @article.update(title: 'A new title') 
	# Rails would only update the title attribute, leaving all other attributes untouched.
	
  
	
	
	#-----------------------------------STEP 6--------------------------------------------------
	# We use the delete method for destroying resources, and this route is mapped to the destroy action inside app/controllers/articles_controller.rb, 
	# which doesn't exist yet. The destroy method is generally the last CRUD action in the controller, and like the other public CRUD actions, 
	# it must be placed before any private or protected methods.
	    def destroy
               @article = Article.find(params[:id])
               @article.destroy
 
                   redirect_to articles_path
         end
	
	# You can call destroy on Active Record objects when you want to delete them from the database. 
	# Note that we don't need to add a view for this action since we're redirecting to the index action.
	 
	
	#---------------------------------VERY IMPORTANT --------------------------------------------
	
	 # Ensure you have a firm grasp of the params method, as you'll use it fairly regularly. 
	 # Let's consider an example URL: http://www.example.com/?username=dhh&email=dhh@email.com. 
	 # In this URL, params[:username] would equal "dhh" and params[:email] would equal "dhh@email.com".
	 # You have to put the edit and update methods above private. 
	 # Anything below private will be a private method and edit and update should be public methods.
 
	private
		def article_params
		params.require(:article).permit(:title, :text)
		end	
	 
	
	 
	 # Here's what's going on: every Rails model can be initialized with its respective attributes, which are automatically mapped to the respective database columns. 
	 # In the first line we do just that (remember that params[:article] contains the attributes we're interested in).
	 # Then, @article.save is responsible for saving the model in the database.  
	 # Next:  We have to whitelist our controller parameters to prevent wrongful mass assignment. 
	 # In this case, we want to both allow and require the title and text parameters for valid use of create. 
	 # The syntax for this introduces require and permit. The change will involve one line in the create action:

       #  @article = Article.new(params.require(:article).permit(:title, :text))
end