class Article < ActiveRecord::Base
	# This is an Article Model ---------------------------------------------
	
	# Models in Rails use a singular name, and their corresponding database tables use a plural name. 
	# Rails provides a generator for creating models, which most Rails developers tend to use when creating new models.
	# To create the new model, run this command in your terminal:
	
	# $ bin/rails generate model Article title:string text:text
	
	# With that command we told Rails that we want a Article model, together with a title attribute of type string, and a text attribute of type text. 
	# Those attributes are automatically added to the articles table in the database and mapped to the Article model.
	
	# Active Record is smart enough to automatically map column names to model attributes, 
	# which means you don't have to declare attributes inside Rails models, as that will be done automatically by Active Record.
	# We have just created this file + a  database migration file inside the db/migrate directory.
	
	
	
	# There isn't much to this file - but note that the Article class inherits from ActiveRecord::Base. 
	# Active Record supplies a great deal of functionality to your Rails models for free, including basic database CRUD (Create, Read, Update, Destroy) operations, 
	# data validation, as well as sophisticated search support and the ability to relate multiple models to one another.
    # Rails includes methods to help you validate the data that you send to models. Open the app/models/article.rb file 
	
	        has_many :comments, dependent: :destroy
			validates :title, presence: true,
                    length: { minimum: 5 }
	
	 
	 # These changes will ensure that all articles have a title that is at least five characters long. Rails can validate a variety of conditions in a model, 
	 # including the presence or uniqueness of columns, their format, and the existence of associated objects.
	 
	 # With the validation now in place, when you call @article.save on an invalid article, it will return false. 
	 # If you open app/controllers/articles_controller.rb again, you'll notice that we don't check the result of calling @article.save inside the create action.
	 
	 # If you delete an article, its associated comments will also need to be deleted, otherwise they would simply occupy space in the database. Rails allows you to use the dependent option of an association to achieve this. Modify the Article model, app/models/article.rb, as follows:

							#class Article < ActiveRecord::Base
								#  has_many :comments, dependent: :destroy
								# validates :title, presence: true,
											# length: { minimum: 5 }
end


