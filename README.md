# Tiramisu

> Dessert by Russian Doll

![image](https://raw.githubusercontent.com/Gochojr/Tiramisu/master/Tiramisu_with_blueberries_and_raspberries.png)

## Introduction

:beginner:  :icecream: :coffee: :cake:

[Tiramisu](http://gochojr.github/Tiramisu) is a Complete Beginner's Example of - [Getting Started with Ruby on Rails](http://guides.rubyonrails.org/getting_started.html)

## Getting Started

The ingredients by [Russian Doll](https://signalvnoise.com/posts/3690-the-performance-impact-of-russian-doll-caching) are:

- [x] [Ruby 2.0.0p576](http://rubyinstaller.org/downloads/) 
- [x] [Rails 4.2.5](http://rubyonrails.org/download/)
- [x] [SQlite3 3.9.2](https://www.sqlite.org/download.html)
- [x] [ExecJS](https://github.com/rails/execjs#readme)


## Steps after Installation

Considering that you have all these ingredients installed :globe_with_meridians: globally or in user: 

**Step 1:** Using your favourite BASH emulation say [GitBash](https://git-for-windows.github.io/) and `cd` to your directory where the dessert is packed and run the following command: 

`ruby bin\rails server`

| The WEBRICK server will initiate at `http://localhost:3000/`

**Step 2:** Test your application which is a blog by going to `http://localhost:3000/articles/`. Here you should get a listing of all Articles [Tiramisu](http://gochojr.github/Tiramisu) comes with. Test the `show`, `edit` and `destroy` commands on the blog.

**Step 3:** Refer to the documentations in resources below and look at  [Tiramisu](http://gochojr.github/Tiramisu) ruby code which has descriptions and well laid explanations of the steps and as what is happening!


## Contributing
In lieu of the steps provided in [Getting Started with Rails](http://guides.rubyonrails.org/getting_started.html) any commit requests must abide!

_Also, please don't edit files in bin directory!_


## Resources 

* [Getting Started with Rails](http://guides.rubyonrails.org/getting_started.html)
* [Install Rails](http://installrails.com/)

## Help

Contact the [repo maintainer](http://torsybil.github.io/)

Find Tiramisu in [GitHub](https://github.com/torsybil/Tiramisu)


## License

Copyright (c) 2015 Joseph Godwin Kimani  

Licensed under the MIT license.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.